CREATE DATABASE  IF NOT EXISTS `frauddb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `frauddb`;


  DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frauddb`.`clients` (
  `client_id` INT NOT NULL AUTO_INCREMENT,
  `client_short_name` VARCHAR(45) NOT NULL,
  `client_full_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`client_id`),
  UNIQUE INDEX `id_UNIQUE` (`client_id` ASC));
  
  
DROP TABLE IF EXISTS `risk_persons`;
CREATE TABLE `frauddb`.`risk_persons` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rp_first_name` VARCHAR(45) NOT NULL,
  `rp_middle_name` VARCHAR(45) NULL,
  `rp_last_name` VARCHAR(45) NOT NULL,
  `rp_address_1` VARCHAR(45) NOT NULL,
  `rp_address_2` VARCHAR(45) NULL,
  `rp_region_state` VARCHAR(45) NULL,
  `rp_city` VARCHAR(45) NOT NULL,
  `rp_zip_index` VARCHAR(10) NOT NULL,
  `rp_country` VARCHAR(45) NOT NULL,
  `rp_reason` TEXT NOT NULL,
  `rp_comment` VARCHAR(45) NULL,
  `rp_added_by_id` INT NOT NULL,
  `rp_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  
  KEY `FK_CLIENT_idx` (`rp_added_by_id`),
  
  CONSTRAINT `FK_CLIENT` 
  FOREIGN KEY (`rp_added_by_id`) 
  REFERENCES `clients` (`client_id`) 
  
  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
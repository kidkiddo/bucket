<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 10.04.2018
  Time: 16:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>List RiskPersons</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="/resources/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css"
          integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
</head>
<body>

<form:form>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>FPDB</h3>
            </div>

            <ul class="list-unstyled components">
                <p>
                    <security:authorize access="!isAuthenticated()">
                        <a class="btn btn-primary" href="/login" role="button">Login</a>
                    </security:authorize>
                    <security:authorize access="isAuthenticated()">
                        <form:form cssClass="form-inline m-0">
                    <span style="margin:5px">Hello, <security:authentication
                            property="principal.username"/></span>
                            <button class="btn btn-warning" style="margin:5px"
                                    formaction="${pageContext.request.contextPath}/logout" formmethod="post">Logout
                            </button>
                        </form:form>
                    </security:authorize>
                </p>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">I'm <security:authentication
                            property="principal.username"/> !</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="${pageContext.request.contextPath}/myAccount">My Account</a>
                        </li>
                        <form:form action="${pageContext.request.contextPath}/logout" method="post" name="logoutForm">
                            <li>
                                <a href="#" onclick="document.logoutForm.submit()">Logout</a>
                            </li>
                        </form:form>
                    </ul>
                </li>
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">RiskPersons</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li><a href="/persons/showAll">Show random</a></li>
                        <li><a href="/persons/showAddedByMe">Show added by me</a></li>
                        <li><a href="/persons/add">Add New</a></li>
                        <li><a href="/persons/check">Check</a></li>
                    </ul>
                </li>
                <security:authorize access="hasAnyRole('ADMIN','MANAGER')">
                    <li>
                        <a href="/clients/showAll">Clients</a>
                    </li>
                </security:authorize>
            </ul>

            <ul class="list-unstyled CTAs">
                <li>
                    <a href="/contactUs">Contact Us</a>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span>Menu</span>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <form:form action="searchInPeople" method="post" cssClass="form-inline m-0">
                                <input class="form-control mr-2" type="text" name="search" placeholder="Search">
                                <button class="btn btn-info navbar-btn" type="submit">Search</button>
                            </form:form>

                            <!--
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                            -->
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- <div class="py-5">-->
            <div class="container">
                <div class="row">
                    <!--<div class="col-md-20">-->
                    <a class="btn btn-primary" href="/persons/add" role="button">Add Person</a>
                    <a class="btn btn-warning" href="/persons/check" role="button">Check Person</a>
                    <a class="btn btn-primary" href="/persons/showAddedByMe" role="button">Added By Me</a>
                    <span class="text-dark text-center">20 Random persons</span>
                    <br><br>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>id</th>
                            <th>Email</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th>Address 1</th>
                            <th>State</th>
                            <th>City</th>
                            <th>ZIP</th>
                            <th>Country</th>
                            <th>Reason</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${!empty persons}">
                            <c:forEach var="tempPerson" items="${persons}">
                                <c:url var="deleteLink" value="/persons/delete">
                                    <c:param name="id" value="${tempPerson.id}"/>
                                </c:url>
                                <c:url var="updateLink" value="/persons/update">
                                    <c:param name="id" value="${tempPerson.id}"/>
                                </c:url>
                                <c:url var="detailsLink" value="/persons/showDetails">
                                    <c:param name="id" value="${tempPerson.id}"/>
                                </c:url>
                                <tr>
                                    <td>
                                        <a href="${detailsLink}" class="fas fa-user fa-sm"></a>
                                        <a href="${updateLink}" class="fas fa-pencil-alt fa-sm"></a>
                                        <a href="${deleteLink}" class="fas fa-trash-alt fa-sm"
                                           onclick="if (!(confirm('Are you sure u want to delete this client'))) return false"></a>
                                    </td>
                                    <td>${tempPerson.id}</td>
                                    <td>${tempPerson.email}</td>
                                    <td>${tempPerson.firstName}</td>
                                    <td>${tempPerson.middleName}</td>
                                    <td>${tempPerson.lastName}</td>
                                    <td>${tempPerson.address1}</td>
                                    <td>${tempPerson.region_state}</td>
                                    <td>${tempPerson.city}</td>
                                    <td>${tempPerson.zip}</td>
                                    <td>${tempPerson.country}</td>
                                    <td>${tempPerson.reason}</td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                    <!--</div>-->
                </div>
            </div>
            <!--</div>-->
        </div>
    </div>
</form:form>

<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>
</html>

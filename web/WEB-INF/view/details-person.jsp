<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 10.04.2018
  Time: 22:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>List RiskPersons</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="/resources/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css"
          integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
</head>
<body>
<form:form>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>FPDB</h3>
            </div>

            <ul class="list-unstyled components">
                <p>
                    <security:authorize access="!isAuthenticated()">
                        <a class="btn btn-primary" href="/login" role="button">Login</a>
                    </security:authorize>
                    <security:authorize access="isAuthenticated()">
                        <form:form cssClass="form-inline m-0">
                    <span style="margin:5px">Hello, <security:authentication
                            property="principal.username"/></span>
                            <button class="btn btn-warning" style="margin:5px"
                                    formaction="${pageContext.request.contextPath}/logout" formmethod="post">Logout
                            </button>
                        </form:form>
                    </security:authorize>
                </p>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">I'm <security:authentication
                            property="principal.username"/> !</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="${pageContext.request.contextPath}/myAccount">My Account</a>
                        </li>
                        <form:form action="${pageContext.request.contextPath}/logout" method="post" name="logoutForm">
                            <li>
                                <a href="#" onclick="document.logoutForm.submit()">Logout</a>
                            </li>
                        </form:form>
                    </ul>
                </li>
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">RiskPersons</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li><a href="/persons/showAll">Show random</a></li>
                        <li><a href="/persons/showAddedByMe">Show added by me</a></li>
                        <li><a href="/persons/add">Add New</a></li>
                        <li><a href="/persons/check">Check</a></li>
                    </ul>
                </li>
                <security:authorize access="hasAnyRole('ADMIN','MANAGER')">
                    <li>
                        <a href="/clients/showAll">Clients</a>
                    </li>
                </security:authorize>
            </ul>

            <ul class="list-unstyled CTAs">
                <li>
                    <a href="/contactUs">Contact Us</a>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span>Menu</span>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <form:form action="searchInPeople" method="post" cssClass="form-inline m-0">
                                <input class="form-control mr-2" type="text" name="search" placeholder="Search">
                                <button class="btn btn-info navbar-btn" type="submit">Search</button>
                            </form:form>

                            <!--
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                            <li><a href="#">Page</a></li>
                            -->
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img class="img-fluid d-block" src="/resources/img/person_icon.png">
                    </div>
                    <div class="col-md-4">
                        <form:form action="showDetails" modelAttribute="person" method="get">
                        <form:hidden path="id"/>
                        <c:url var="deleteLink" value="/persons/delete">
                            <c:param name="id" value="${person.id}"/>
                        </c:url>
                        <c:url var="updateLink" value="/persons/update">
                            <c:param name="id" value="${person.id}"/>
                        </c:url>
                        <h1 class="" contenteditable="true"><b>RiskPerson id${person.id} details</b></h1>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="">
                                    <li>${person.email}</li>
                                    <li>${person.firstName}</li>
                                    <li>${person.middleName}</li>
                                    <li>${person.lastName}</li>
                                    <li>${person.address1}</li>
                                    <li>${person.city}</li>
                                    <li>${person.region_state}</li>
                                    <li>${person.zip}</li>
                                    <li>${person.country}</li>
                                    <li>${person.reason}</li>
                                    <li>${person.comment}</li>
                                    <li>${person.timestamp}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-warning" href="${updateLink}">Update </a>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-danger"
                                   onclick="if (!(confirm('Are you sure u want to delete this client'))) return false"
                                   href="${deleteLink}">Delete </a>
                            </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form:form>
<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>
</html>

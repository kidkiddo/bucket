<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 11.03.2018
  Time: 14:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>FPDB</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          type="text/css">
    <link rel="stylesheet" href="https://v40.pingendo.com/assets/4.0.0/default/theme.css" type="text/css">

</head>
</head>
<body>
<nav class="navbar navbar-expand-md bg-secondary navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="/home">Dashboard</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/clients/showAll">Clients</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/persons/showAll">RiskList</a>
                </li>
            </ul>
            <security:authorize access="!isAuthenticated()">
                <a class="btn btn-primary" href="/login" role="button">Login</a>
            </security:authorize>
            <security:authorize access="isAuthenticated()">
                <form:form cssClass="form-inline m-0">
                    <span class="navbar-text" style="margin:5px" >Hello, <security:authentication property="principal.username"/></span>
                    <button class="btn btn-warning" style="margin:5px" formaction="${pageContext.request.contextPath}/logout" formmethod="post">Logout</button>
                </form:form>
            </security:authorize>
        </div>
    </div>
</nav>
<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="display-1">
                    <div style="text-align: center;"><b style="color: inherit; font-size: 6rem;">Fraud People Database&nbsp;</b>
                        <font color="#ffffff"><span
                                style="font-size: 72px; white-space: nowrap; background-color: rgb(18, 187, 173);"><b>0.1</b></span></font>
                    </div>
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="">
    <div class="container">
        <div class="row">
            <div class="col-md-4 align-self-center text-center p-4"><i class="d-block  fa fa-5x fa-font-awesome"></i>
                <h3 class="my-2">Cool API</h3>
                <p class="">Simple and cool API for integration into your project!</p>
            </div>
            <div class="col-md-4 align-self-center text-center p-4"><i class="d-block  fa fa-5x fa-font-awesome"></i>
                <h3 class="my-2">Tremendous Database</h3>
                <p class="">More then 1000 records all over the world!</p>
            </div>
            <div class="col-md-4 align-self-center text-center p-4"><i class="d-block  fa fa-5x fa-font-awesome"></i>
                <h3 class="my-2">Cheap prices</h3>
                <p class="">Only 0.2$ per check. Various of different plans!</p>
            </div>
        </div>
    </div>
</div>
<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12"></div>
        </div>
    </div>
</div>
<div class="py-5">
    <div class="container">
        <div class="row"></div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>

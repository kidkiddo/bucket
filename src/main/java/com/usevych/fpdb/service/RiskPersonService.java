package com.usevych.fpdb.service;

import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.RiskPerson;
import com.usevych.fpdb.entities.RiskPersonMatchResult;

import javax.validation.Valid;
import java.util.List;

public interface RiskPersonService {

    public List<RiskPerson> getAllPersons();

    public void savePerson(RiskPerson riskPerson);

    public void deletePerson(int id);

    public RiskPerson getPerson(int id);

    public List<RiskPerson> getPersonsByClient(Client client);

    public List<RiskPerson> searchRiskPersons(String keyword);

    public RiskPersonMatchResult searchSimilarPeople(RiskPerson riskPerson);

    public List<RiskPerson> getRandomPersons();

}

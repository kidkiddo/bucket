package com.usevych.fpdb.service;

import com.usevych.fpdb.dao.ClientDAO;
import com.usevych.fpdb.dao.UserDAO;
import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;



@Service
public class ClientServiceImpl implements ClientService{

    @Autowired
    private ClientDAO clientDAO;

    @Autowired
    private UserDAO userDAO;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    @Transactional
    public List<Client> getClients() {
        return clientDAO.getClients();
    }

    @Override
    @Transactional
    public void saveClient(Client client) {
        if (userDAO.userExists(client.getUserClient().getUserName())) {
            //PASSWORD EMPTY - LEFT THE SAME
            if (client.getUserClient().getPassword() == null) {
                client.getUserClient().setPassword(userDAO.getUserClientPassword(client.getUserClient().getUserName()));
                clientDAO.saveClient(client);
            } else {
                // PASSWORD FULFILLED - CHANGE CURRENT PASSWORD
                String plainPassword = client.getUserClient().getPassword();
                String encodedPassword = passwordEncoder.encode(plainPassword);
                encodedPassword = "{bcrypt}" + encodedPassword;
                UserClient userClient = client.getUserClient();
                userClient.setPassword(encodedPassword);
                userClient.setEnabled(true);
                userDAO.updateUserClient(userClient);
                clientDAO.saveClient(client);
            }
        } else {
            // USER NOT EXISTS - CREATING A NEW ONE
            if (!client.getUserClient().getPassword().isEmpty()) {
                String encodedPassword = passwordEncoder.encode(client.getUserClient().getPassword());
                encodedPassword = "{bcrypt}" + encodedPassword;
                client.getUserClient().setPassword(encodedPassword);
                List<GrantedAuthority> authorities =
                        AuthorityUtils.createAuthorityList("ROLE_CLIENT");
                User user = new User(client.getUserClient().getUserName(), encodedPassword, authorities);
                userDAO.createUser(user);
                clientDAO.saveClient(client);
            }
        }
        clientDAO.saveClient(client);
    }

    @Override
    @Transactional
    public void deleteClient(int id) {
        UserClient userClient = clientDAO.getClient(id).getUserClient();
        Client client = clientDAO.getClient(id);
        userDAO.deleteUser(userClient.getUserName());
    }

    @Override
    @Transactional
    public Client getClient(int id) {
        return clientDAO.getClient(id);
    }

    @Override
    @Transactional
    public List<Client> searchClients(String keyword) {
        return clientDAO.searchClients(keyword);
    }

    @Override
    @Transactional
    public void incrementClientPeople(int id) {
        clientDAO.incrementClientPeople(id);
    }

    @Override
    @Transactional
    public void decrementClientPeople(int id) {
        clientDAO.decrementClientPeople(id);
    }

    @Override
    @Transactional
    public void decrementClientCredits(Client client) {
        client.setCredits(client.getCredits()-1);
        clientDAO.saveClient(client);
    }

}

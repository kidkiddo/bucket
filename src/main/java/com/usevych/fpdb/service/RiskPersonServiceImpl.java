package com.usevych.fpdb.service;

import com.usevych.fpdb.checker.PersonChecker;
import com.usevych.fpdb.dao.RiskPersonDAO;
import com.usevych.fpdb.dao.UserDAO;
import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.RiskPerson;
import com.usevych.fpdb.entities.RiskPersonMatchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;


@Service
public class RiskPersonServiceImpl implements RiskPersonService {

    @Autowired
    private RiskPersonDAO riskPersonDAO;


    @Autowired
    private PersonChecker personChecker;

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public List<RiskPerson> getAllPersons() {
        return riskPersonDAO.getAllRiskPersons();
    }

    @Override
    @Transactional
    public List<RiskPerson> getRandomPersons() {
        return riskPersonDAO.getRandomPersons(20);
    }


    @Override
    @Transactional
    public void savePerson(RiskPerson riskPerson) {
        Date date = new Date();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication(); //
        Client client = userDAO.getClientByUsername(authentication.getName());              //
        riskPerson.setClient(client);
        riskPersonDAO.saveRiskPerson(riskPerson);
    }

    @Override
    @Transactional
    public void deletePerson(int id) {
        if (riskPersonDAO.personExists(id)) {              //
            riskPersonDAO.deleteRiskPerson(id);
        }

    }

    @Override
    @Transactional
    public RiskPerson getPerson(int id) {
        return riskPersonDAO.getRiskPerson(id);
    }

    @Override
    @Transactional
    public List<RiskPerson> getPersonsByClient(Client client) {
        return riskPersonDAO.getRiskPersonsByClient(client);
    }

    @Override
    @Transactional
    public List<RiskPerson> searchRiskPersons(String keyword) {
        return riskPersonDAO.searchRiskPersons(keyword);
    }

    @Override
    @Transactional
    public RiskPersonMatchResult searchSimilarPeople(RiskPerson riskPerson) {
        double max = 0;
        double interm = 0;
        RiskPersonMatchResult finalResultObj = new RiskPersonMatchResult();
        RiskPersonMatchResult tempResultObj = new RiskPersonMatchResult();
        List<RiskPerson> people = riskPersonDAO.searchSimilarPeople(riskPerson); //
        System.out.println("FOUND : " + people.size());
        for (RiskPerson currentPerson : people) {
            tempResultObj = personChecker.getMatch(riskPerson, currentPerson);
            interm = tempResultObj.getMainMatchResult();
            if (interm >= max) {
                max = interm;
                finalResultObj.setMainMatchResult(tempResultObj.getMainMatchResult());
                finalResultObj.setAddressMatchResult(tempResultObj.getAddressMatchResult());
                finalResultObj.setRiskPerson(currentPerson);
            }
        }
        System.out.println(finalResultObj.getMainMatchResult());
        return finalResultObj;
    }
}

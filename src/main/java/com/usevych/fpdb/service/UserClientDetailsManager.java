package com.usevych.fpdb.service;

import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.UserClient;
import org.hibernate.Session;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Query;
import javax.transaction.Transactional;

public interface UserClientDetailsManager {

    public Client getClientByUsername(String login) ;

    public UserClient getUserByClientID(int id) ;


    public boolean isUserExists(String username) ;


    public String getUserClientPassword(String username) ;


    public void deleteUserClient(UserClient userClient);


    public void updateUserClient(UserClient userClient) ;



    public void createUser(UserDetails user) ;


    public void updateUser(UserDetails user);


    public void deleteUser(String username);


    public void changePassword(String oldPassword, String newPassword) throws AuthenticationException;


    public boolean userExists(String username);
}

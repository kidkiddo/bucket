package com.usevych.fpdb.service;

import com.usevych.fpdb.entities.Client;

import java.util.List;

public interface ClientService {

    public List<Client> getClients();

    public void saveClient(Client client);

    public void deleteClient(int id);

    public Client getClient(int id);

    public List<Client> searchClients(String keyword);

    public void incrementClientPeople(int id);

    public void decrementClientPeople(int id);

    public void decrementClientCredits(Client client);

}

package com.usevych.fpdb.service;

import com.usevych.fpdb.dao.UserDAO;
import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UserClientDetailsManagerImpl implements UserClientDetailsManager {

    @Autowired
    private UserDAO userDAO;

    @Override
    public Client getClientByUsername(String login) {
        return userDAO.getClientByUsername(login);
    }

    @Override
    public UserClient getUserByClientID(int id) {
        return userDAO.getUserByClientID(id);
    }

    @Override
    public boolean isUserExists(String username) {
        return userDAO.isUserExists(username);
    }

    @Override
    public String getUserClientPassword(String username) {
        return userDAO.getUserClientPassword(username);
    }

    @Override
    public void deleteUserClient(UserClient userClient) {
        userDAO.deleteUserClient(userClient);
    }

    @Override
    public void updateUserClient(UserClient userClient) {
        userDAO.updateUserClient(userClient);
    }

    @Override
    public void createUser(UserDetails user) {
        userDAO.createUser(user);
    }

    @Override
    public void updateUser(UserDetails user) {
        userDAO.updateUser(user);
    }

    @Override
    public void deleteUser(String username) {
        userDAO.deleteUser(username);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) throws AuthenticationException {
        userDAO.changePassword(oldPassword, newPassword);
    }

    @Override
    public boolean userExists(String username) {
        return userDAO.userExists(username);
    }
}

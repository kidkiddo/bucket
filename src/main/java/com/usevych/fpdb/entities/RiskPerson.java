package com.usevych.fpdb.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 * Entity class that store sensitive information about possible fraud/risk person.
 * Has ManyToOne relationships with Clients
 */
@Entity
@Table(name = "risk_persons")
public class RiskPerson implements Cloneable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "rp_first_name")
    @NotNull(message = "is required")
    private String firstName;

    @Column(name = "rp_middle_name")
    private String middleName;

    @Column(name = "rp_last_name")
    @NotNull(message = "is required")
    private String lastName;

    @Column(name = "rp_address_1")
    @NotNull(message = "is required")
    private String address1;

    @Column(name = "rp_address_2")
    private String address2;

    @Column(name = "rp_region_state")
    private String region_state;

    @Column(name = "rp_city")
    private String city;

    @Column(name = "rp_zip_index")
    @NotNull(message = "is required")
    private String zip;

    @Column(name = "rp_country")
    @NotNull(message = "is required")
    private String country;

    @Column(name = "rp_reason")
    @NotNull(message = "is required")
    private String reason;

    @Column(name = "rp_comment")
    private String comment;

    @Column(name = "rp_email")
    @Email(message = "Invalid Email")
    @NotNull(message = "is required")
    private String email;

    @Column(name = "rp_date")
    private Date timestamp;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "rp_added_by_id")
    @JsonIgnore
    private Client client;

    public RiskPerson() {
    }

    public RiskPerson(String firstName, String middleName, String lastName, String address1, String address2,
                      String region_state, String city, String zip, String country, String reason, String comment) {
        Date date = new java.util.Date();
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.address1 = address1;
        this.address2 = address2;
        this.region_state = region_state;
        this.city = city;
        this.zip = zip;
        this.country = country;
        this.reason = reason;
        this.comment = comment;
        this.timestamp = new Timestamp(System.currentTimeMillis());

    }

    public RiskPerson(@NotNull(message = "is required") String firstName, String middleName, @NotNull(message = "is required") String lastName, @NotNull(message = "is required") String address1, String address2, String region_state, String city, @NotNull(message = "is required") String zip, @NotNull(message = "is required") String country, @NotNull(message = "is required") String reason, String comment, @Email(message = "Invalid Email") @NotNull(message = "is required") String email) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.address1 = address1;
        this.address2 = address2;
        this.region_state = region_state;
        this.city = city;
        this.zip = zip;
        this.country = country;
        this.reason = reason;
        this.comment = comment;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getRegion_state() {
        return region_state;
    }

    public void setRegion_state(String region_state) {
        this.region_state = region_state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    /*
    public int getAdded_by() {
        return added_by;
    }

    public void setAdded_by(int added_by) {
        this.added_by = added_by;
    }
    */
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "RiskPerson{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", region_state='" + region_state + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                ", country='" + country + '\'' +
                ", reason='" + reason + '\'' +
                ", comment='" + comment + '\'' +
                ", email='" + email + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RiskPerson person = (RiskPerson) o;
        return id == person.id &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(middleName, person.middleName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(address1, person.address1) &&
                Objects.equals(address2, person.address2) &&
                Objects.equals(region_state, person.region_state) &&
                Objects.equals(city, person.city) &&
                Objects.equals(zip, person.zip) &&
                Objects.equals(country, person.country) &&
                Objects.equals(reason, person.reason) &&
                Objects.equals(comment, person.comment) &&
                Objects.equals(email, person.email) &&
                Objects.equals(timestamp, person.timestamp) ;//&&
                //Objects.equals(client, person.client);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, middleName, lastName, address1, address2, region_state, city, zip, country, reason, comment, email, timestamp /*, client*/);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

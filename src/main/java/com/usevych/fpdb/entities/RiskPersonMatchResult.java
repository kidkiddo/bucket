package com.usevych.fpdb.entities;

/**
 *  Object to return the result of checking operations
 */
public class RiskPersonMatchResult {

    private double mainMatchResult;

    private double addressMatchResult;

    private RiskPerson riskPerson;

    public RiskPersonMatchResult(double mainMatchResult, RiskPerson riskPerson) {
        this.mainMatchResult = mainMatchResult;
        this.riskPerson = riskPerson;
    }

    public RiskPersonMatchResult(double mainMatchResult, double addressMatchResult) {
        this.mainMatchResult = mainMatchResult;
        this.addressMatchResult = addressMatchResult;
    }

    public RiskPersonMatchResult() {
    }

    public RiskPersonMatchResult(double mainMatchResult, double addressMatchResult, RiskPerson riskPerson) {
        this.mainMatchResult = mainMatchResult;
        this.addressMatchResult = addressMatchResult;
        this.riskPerson = riskPerson;
    }

    public double getMainMatchResult() {
        return mainMatchResult;
    }

    public void setMainMatchResult(double mainMatchResult) {
        this.mainMatchResult = mainMatchResult;
    }

    public double getAddressMatchResult() {
        return addressMatchResult;
    }

    public void setAddressMatchResult(double addressMatchResult) {
        this.addressMatchResult = addressMatchResult;
    }

    public RiskPerson getRiskPerson() {
        return riskPerson;
    }

    public void setRiskPerson(RiskPerson riskPerson) {
        this.riskPerson = riskPerson;
    }

    @Override
    public String toString() {
        return "RiskPersonMatchResult{" +
                "mainMatchResult=" + mainMatchResult +
                ", addressMatchResult=" + addressMatchResult +
                ", riskPerson=" + riskPerson +
                '}';
    }
}

package com.usevych.fpdb.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This class is the security part of Client Entity. It's store user credentials
 */
@Entity
@Table(name = "users")
public class UserClient {

    @Id
    @Column(name = "username")
    @NotNull(message = "is required")
    @Size(min = 1, message = "is required")
    private String userName;

    @Column(name = "password")
    @NotNull
    @Size(min = 4, message = "min 4 symbols required")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;


    @OneToOne(mappedBy = "userClient", cascade = CascadeType.ALL)
    private Client client;


    public UserClient(@NotNull(message = "is required") @Size(min = 1, message = "is required") String userName, @NotNull @Size(min = 4, message = "min 4 symbols required") String password) {
        this.userName = userName;
        this.password = password;
    }


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public UserClient() {
    }

    @Override
    public String toString() {
        return "UserClient{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

package com.usevych.fpdb.entities;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.TermVector;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Entity class to store additional information about Client
 *
 * Has OneToMany relationships with RiskPerson
 * Has OneToOne (Bi-directional) relationships with UserClient
 */
@Entity
@Table(name = "clients")
@Indexed
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id") // LONG INSTEAD OF INT
    private int id;

    @Column(name = "client_short_name")
    @Field(termVector = TermVector.YES)
    @NotNull(message = "is required")
    private String shortName;

    @Column(name = "client_full_name")
    @Field(termVector = TermVector.YES)
    @NotNull(message = "is required")
    private String fullName;

    @Column(name = "client_rp_count")
    private int countAddedPeople;

    @Column(name = "client_credits")
    @Min(1)
    private int credits;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "client", cascade = CascadeType.ALL)
    private List<RiskPerson> riskPeople;

    @OneToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "user")
    private UserClient userClient;




    public void add(RiskPerson riskPerson) {
        if (riskPeople == null) {
            riskPeople = new ArrayList<>();
        }
        //riskPerson.setAdded_by(this.getId());
        riskPeople.add(riskPerson);
        riskPerson.setClient(this);
    }

    public Client() {
    }

    public Client(String shortName, String fullName) {
        this.shortName = shortName;
        this.fullName = fullName;
    }

    public Client(@NotNull(message = "is required") String shortName, @NotNull(message = "is required") String fullName, UserClient userClient) {
        this.shortName = shortName;
        this.fullName = fullName;
        this.userClient = userClient;
    }

    public Client(@NotNull(message = "is required") String shortName, @NotNull(message = "is required") String fullName, int credits) {
        this.shortName = shortName;
        this.fullName = fullName;
        this.credits = credits;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public UserClient getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClient userClient) {
        this.userClient = userClient;
    }

    public int getCountAddedPeople() {
        return countAddedPeople;
    }

    public void setCountAddedPeople(int countAddedPeople) {
        this.countAddedPeople = countAddedPeople;
    }

    public List<RiskPerson> getRiskPeople() {
        return riskPeople;
    }

    public void setRiskPeople(List<RiskPerson> riskPeople) {
        this.riskPeople = riskPeople;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                Objects.equals(shortName, client.shortName) &&
                Objects.equals(fullName, client.fullName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, shortName, fullName);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", shortName='" + shortName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", countAddedPeople=" + countAddedPeople +
                ", credits=" + credits +
                '}';
    }
}

package com.usevych.fpdb.controller;

import com.usevych.fpdb.dao.UserDAO;
import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.UserClient;
import com.usevych.fpdb.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/clients")
public class ClientController {

    @Autowired
    private ClientService clientService;



    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor trimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, trimmerEditor);
    }

    @GetMapping(value = "/showAll")
    public String showClients(Model model) {
        List<Client> clients = clientService.getClients();
        model.addAttribute("clients", clients);
        return "list-clients";
    }


    @GetMapping(value = "/add")
    public String addClient(Model model) {
        Client client = new Client();
        UserClient userClient = new UserClient();
        client.setUserClient(userClient);
        model.addAttribute("client", client);
        return "add-client";
    }

    @PostMapping(value = "/saveClient")
    public String saveClient(@Valid @ModelAttribute("client") Client client,
                             BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "add-client";
        }
        clientService.saveClient(client);
        final String message = "Client " + client.getUserClient().getUserName() + " updated";
        model.addAttribute("message ", message);
        return "redirect:/clients/showAll";
    }

    @GetMapping(value = "/delete")
    public String deleteClient(@ModelAttribute("clientId") int id) {
        clientService.deleteClient(id);
        return "redirect:/clients/showAll";
    }

    @GetMapping(value = "/update")
    public String updateClient(@ModelAttribute("clientId") int id, Model model) {
        Client client = clientService.getClient(id);
        client.getUserClient().setPassword("");
        model.addAttribute("client", client);
        return "add-client";
    }

    @PostMapping(value = "/searchInClients")
    public String searchClient(@ModelAttribute(value = "search") String keyword, Model model) {
        List<Client> clients = clientService.searchClients(keyword);
        model.addAttribute("clients", clients);
        return "list-clients";
    }


    @GetMapping(value = "/showClientDetails")
    public String showClientDetails(@ModelAttribute("clientId") int id, Model model) {
        Client client = clientService.getClient(id);
        model.addAttribute("client", client);
        return "details-client";
    }

}

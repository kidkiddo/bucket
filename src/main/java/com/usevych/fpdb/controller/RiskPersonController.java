package com.usevych.fpdb.controller;

import com.usevych.fpdb.dao.UserDAO;
import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.RiskPerson;
import com.usevych.fpdb.entities.RiskPersonMatchResult;
import com.usevych.fpdb.service.ClientService;
import com.usevych.fpdb.service.RiskPersonService;
import com.usevych.fpdb.service.UserClientDetailsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping(value = "/persons")
public class RiskPersonController {

    @Autowired
    private RiskPersonService riskPersonService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private UserClientDetailsManager userClientDetailsManager;


    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor trimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, trimmerEditor);
    }

    @GetMapping(value = "/showAll")
    public String showPersons(Model model) {
        List<RiskPerson> persons = riskPersonService.getRandomPersons();
        model.addAttribute("persons", persons);
        return "list-persons"; // ATTENTION
    }

    @GetMapping(value = "/showAddedByMe")
    public String showPersonsAddedByMe(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<RiskPerson> people;
        Client client = userClientDetailsManager.getClientByUsername(authentication.getName());
        people = riskPersonService.getPersonsByClient(client);
        System.out.println(people.size());
        model.addAttribute("persons", people);
        return "list-persons";
    }


    @GetMapping(value = "/searchByClient")
    public String showPersonsByClient(@ModelAttribute("clientId") int id, Model model) {
        Client client = clientService.getClient(id);
        List<RiskPerson> people = riskPersonService.getPersonsByClient(client);
        model.addAttribute("persons", people);
        return "list-persons";
    }

    @PostMapping(value = "/savePerson")
    public String saveRiskPerson(@Valid @ModelAttribute("person") RiskPerson riskPerson,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "add-riskperson";
        } else {
            riskPersonService.savePerson(riskPerson);
            return "redirect:/persons/showAll";
        }
    }

    @GetMapping(value = "/add")
    public String addRiskPerson(Model model) {
        RiskPerson riskPerson = new RiskPerson();
        model.addAttribute("person", riskPerson);
        return "add-riskperson";
    }


    @GetMapping(value = "/delete")
    public String deleteRiskPerson(@ModelAttribute("id") int id) {
        Client client = userClientDetailsManager.getClientByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        RiskPerson riskPerson = riskPersonService.getPerson(id);
        if ((riskPerson.getClient().equals(client)) || (hasRole("ROLE_ADMIN"))) { // CHECKING FOR OWNERSHIP OR AUTHORITIES
            riskPersonService.deletePerson(id);
            return "redirect:/persons/showAll";
        }
        return "access-denied";
    }

    @GetMapping(value = "update")
    public String updateRiskPerson(@ModelAttribute("id") int id, Model model) {
        Client client = userClientDetailsManager.getClientByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        RiskPerson riskPerson = riskPersonService.getPerson(id);
        if (riskPerson.getClient().equals(client) || hasRole("ROLE_ADMIN")) { // CHECKING FOR OWNERSHIP OR AUTHORITIES
            model.addAttribute("person", riskPerson);
            return "add-riskperson";
        }
        return "access-denied";
    }

    @PostMapping(value = "/searchInPeople")
    public String searchPerson(@ModelAttribute("search") String keyword, Model model) {
        List<RiskPerson> people = riskPersonService.searchRiskPersons(keyword);
        model.addAttribute("persons", people);
        return "list-persons";
    }

    @GetMapping(value = "showDetails")
    public String showPersonDetails(@ModelAttribute("id") int id, Model model) {
        RiskPerson person = riskPersonService.getPerson(id);
        model.addAttribute("person", person);
        return "details-person";
    }


    @GetMapping(value = "/check")
    public String checkPerson(Model model) {
        RiskPerson riskPerson = new RiskPerson();
        model.addAttribute("person", riskPerson);
        return "check-riskperson";
    }

    @PostMapping(value = "/startCheckPerson")
    public String startCheckPerson(@Valid @ModelAttribute("person") RiskPerson riskPerson,
                                   BindingResult bindingResult, Model model) {
        if (!hasRole("ROLE_ADMIN")) {
            Client client = userClientDetailsManager.getClientByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            clientService.decrementClientCredits(client);
        }
        if (bindingResult.hasErrors()) {
            return "check-riskperson";
        } else {
            RiskPersonMatchResult RiskPersonMatchResult = new RiskPersonMatchResult();
            RiskPersonMatchResult = riskPersonService.searchSimilarPeople(riskPerson);
            model.addAttribute("matchResult", RiskPersonMatchResult);
            return "check-riskperson-result";
        }
    }

    private boolean hasRole(String role) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }
}

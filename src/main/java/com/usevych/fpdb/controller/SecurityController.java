package com.usevych.fpdb.controller;

import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.UserClient;
import com.usevych.fpdb.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SecurityController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/home")
    public String showHomePage() {
        Client client = new Client();
        client = clientService.getClient(2); //
        return "index";
    }

    @GetMapping("/login")
    public String showLoginPage() {
        return "login";
    }

    @GetMapping("/access-denied")
    public String showAccessDeniedPage() {
        return "access-denied";
    }

    
}

package com.usevych.fpdb.dao;

import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.RiskPerson;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import org.hibernate.query.Query;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Repository
public class RiskPersonDAOImpl implements RiskPersonDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<RiskPerson> getAllRiskPersons() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from RiskPerson");
        return query.getResultList();
    }

    @Override
    public void saveRiskPerson(RiskPerson riskPerson) {
        Session session = sessionFactory.getCurrentSession();
        System.out.println("INSIDE saveRiskPerson");
        session.saveOrUpdate(riskPerson);
    }

    @Override
    public RiskPerson getRiskPerson(int id) {
        Session session = sessionFactory.getCurrentSession();
        RiskPerson riskPerson = session.get(RiskPerson.class, id);
        return riskPerson;
    }

    @Override
    public void deleteRiskPerson(int id) {
        Session session = sessionFactory.getCurrentSession();
        RiskPerson riskPerson = session.get(RiskPerson.class, id);
        session.delete(riskPerson);
    }

    @Override
    public List<RiskPerson> searchRiskPersons(String keyword) {
        Session session = sessionFactory.getCurrentSession();
        Query query = null;
        if (keyword != null && keyword.trim().length() > 0) {
            query = session.createQuery("from RiskPerson where lower(firstName) like :keyword or lower(middleName) like :keyword" +
                    " or lower(lastName) like :keyword or lower(address1) like :keyword or lower(address2) like :keyword " +
                    "or lower(city) like :keyword or lower(region_state) like :keyword or lower(country) like :keyword" +
                    " or lower(zip) like :keyword or lower(reason) like :keyword or lower(email) like :keyword", RiskPerson.class);
            query.setParameter("keyword", "%" + keyword.toLowerCase() + "%");
        } else {
            query = session.createQuery("from RiskPerson ", RiskPerson.class);
        }
        List<RiskPerson> people = query.getResultList();
        return people;
    }

    @Override
    public List<RiskPerson> getRiskPersonsByClient(Client client) {
        int id = client.getId();
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from RiskPerson where rp_added_by_id = " + id);
        return query.getResultList();
    }

    @Override
    //@Transactional
    public boolean personExists(int id) {
        Session session = sessionFactory.getCurrentSession();
        RiskPerson person = session.get(RiskPerson.class, id);
        session.flush();
        session.clear();
        if (person == null)
            return false;
        else
            return true;
    }

    @Override
    @Transactional
    public List<RiskPerson> searchSimilarPeople(RiskPerson person) {
        Session session = sessionFactory.getCurrentSession();
        Query query = null;
        boolean includeCountry = true; //
        String queryString = "from RiskPerson where lower(firstName) like :s_name or lower(lastName) like :f_name " +
                "or lower(email) like :email or lower(address1) like :address1 or lower(address2) like :address2 " +
                "or lower(city) like :city or lower(region_state) like :state or lower(zip) like :zip ";
        if (includeCountry) {
            queryString = queryString + "or lower(country) like :country";
        }
        query = session.createQuery(queryString, RiskPerson.class);
        query.setParameter("s_name", "%" + person.getFirstName() + "%");
        query.setParameter("f_name", "%" + person.getLastName() + "%");
        query.setParameter("address1", "%" + person.getAddress1() + "%");
        query.setParameter("address2", "%" + person.getAddress2() + "%");
        query.setParameter("city", "%" + person.getCity() + "%");
        query.setParameter("state", "%" + person.getRegion_state() + "%");
        query.setParameter("zip", "%" + person.getZip() + "%");
        if (includeCountry)
            query.setParameter("country", "%" + person.getCountry() + "%");
        query.setParameter("email", "%" + person.getEmail() + "%");
        return query.getResultList();
    }

    @Override
    public List<RiskPerson> getRandomPersons(int count) {
        ArrayList<RiskPerson> list = new ArrayList<>();
        int random = 0;

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from RiskPerson order by id DESC");
        query.setMaxResults(1);
        RiskPerson riskPerson = (RiskPerson) query.uniqueResult();
        for (int i = 0; i < count; i++) {
            query = null;
            RiskPerson person = null;
            random = ThreadLocalRandom.current().nextInt(20, riskPerson.getId() + 1);
            query = session.createQuery("from RiskPerson where id=:randomId");
            query.setParameter("randomId", random);
            person = (RiskPerson) query.uniqueResult();
            if (person != null)
                list.add(person);
        }
        return list;
    }

}

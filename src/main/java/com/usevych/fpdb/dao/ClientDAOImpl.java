package com.usevych.fpdb.dao;

import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.RiskPerson;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Repository
public class ClientDAOImpl implements ClientDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Client> getClients() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Client ");
        return query.getResultList();
    }


    @Override
    public void saveClient(Client client) {
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        session.clear();
        session.saveOrUpdate(client);
    }

    @Override
    public void deleteClient(int id) {
        Session session = sessionFactory.getCurrentSession();
        Client client = session.get(Client.class, id);
        session.delete(client);
    }

    @Override
    public Client getClient(int id) {
        Session session = sessionFactory.getCurrentSession();
        Client client = session.get(Client.class, id);
        return client;
    }

    @Override
    public List<Client> searchClients(String keyword) {
        Session session = sessionFactory.getCurrentSession();
        Query query = null;
        if (keyword != null && keyword.trim().length() > 0) {
            query = session.createQuery("from Client where lower(shortName) like :keyword or lower(fullName) like :keyword", Client.class);
            query.setParameter("keyword", "%"+keyword.toLowerCase()+"%");
        } else {
            query = session.createQuery("from Client ", Client.class);
        }
        List<Client> clients = query.getResultList();
        return clients;
    }

    @Override
    public void incrementClientPeople(int id) {
        Session session = sessionFactory.getCurrentSession();
        Client client = session.get(Client.class, id);
        client.setCountAddedPeople(client.getCountAddedPeople()+1);
        session.flush();
    }

    @Override
    public void decrementClientPeople(int id) {
        Session session = sessionFactory.getCurrentSession();
        Client client = session.get(Client.class, id);
        client.setCountAddedPeople(client.getCountAddedPeople()-1);
        session.flush();
    }

    @Override
    public boolean clientExists(int id) {
        Session session = sessionFactory.getCurrentSession();
        Client client = session.get(Client.class, id);
        if (client == null) //
            return false;
        else
            return true;
    }




}

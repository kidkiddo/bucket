package com.usevych.fpdb.dao;

import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.RiskPerson;

import java.util.List;

/**
 *  DAO to work with RiskPerson entity
 */
public interface RiskPersonDAO {

    /**
     * @return all RiskPersons
     */
    public List<RiskPerson> getAllRiskPersons();

    /**
     * Save or update riskperson
     * @param riskPerson
     */
    public void saveRiskPerson(RiskPerson riskPerson);

    /**
     * @param id
     * @return RiskPerson by ID
     */
    public RiskPerson getRiskPerson(int id);

    /**
     * Deleting RiskPerson by ID
     * @param id
     */
    public void deleteRiskPerson(int id);

    /**
     * @param keyword
     * @return List of RiskPersons that contains the keyword in it's vars
     */
    public List<RiskPerson> searchRiskPersons(String keyword);

    /**
     *
     * @param client
     * @return search and return all RiskPersons associated with the Client
     */
    public List<RiskPerson> getRiskPersonsByClient(Client client);

    /**
     * @param id
     * @return is RiskPerson exists with current ID
     */
    public boolean personExists(int id);

    /**
     * Search the similar RiskPersons in DB using the each var of input RiskPerson object
     * @param person
     * @return
     */
    public List<RiskPerson> searchSimilarPeople(RiskPerson person);

    /**
     * Return 20 random RiskPersons for the main page
     * @param count
     * @return
     */
    public List<RiskPerson> getRandomPersons(int count);

}

package com.usevych.fpdb.dao;

import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.UserClient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.transaction.Transactional;

public class UserDAOImpl extends JdbcUserDetailsManager implements UserDAO {


    @Autowired
    private SessionFactory sessionFactory;


    @Transactional
    public Client getClientByUsername(String login) {
        Session session = sessionFactory.getCurrentSession();
        UserClient userClient = session.get(UserClient.class, login);
        System.out.println(userClient.getClient().getId());
        return userClient.getClient();
    }

    @Transactional
    public UserClient getUserByClientID(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from UserClient where client=:id");
        query.setParameter("id",id);
        UserClient userClient = (UserClient) query.getSingleResult();
        return userClient;
    }

    @Transactional
    public boolean isUserExists(String username) {
        Session session = sessionFactory.getCurrentSession();
        UserClient userClient = session.get(UserClient.class, username);
        if (userClient != null)
            return true;
        else
            return false;
    }

    @Transactional
    public String getUserClientPassword(String username) {
        Session session = sessionFactory.getCurrentSession();
        UserClient userClient = session.get(UserClient.class, username);
        return userClient.getPassword();
    }

    @Transactional
    public void deleteUserClient(UserClient userClient) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(userClient);
    }

    @Transactional
    public void updateUserClient(UserClient userClient) {
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        session.update(userClient);
    }


    @Override
    public void createUser(UserDetails user) {
        super.createUser(user);
    }

    @Override
    public void updateUser(UserDetails user) {
        super.updateUser(user);
    }

    @Override
    public void deleteUser(String username) {
        super.deleteUser(username);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) throws AuthenticationException {
        super.changePassword(oldPassword, newPassword);
    }

    @Override
    public boolean userExists(String username) {
        return super.userExists(username);
    }
}

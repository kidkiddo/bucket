package com.usevych.fpdb.dao;

import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.UserClient;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;

public interface UserDAO extends UserDetailsManager {

    public Client getClientByUsername(String login);

    public UserClient getUserByClientID(int id);

    public boolean isUserExists(String username);

    public String getUserClientPassword(String username);

    public void deleteUserClient(UserClient userClient);

    public void updateUserClient(UserClient userClient);

}

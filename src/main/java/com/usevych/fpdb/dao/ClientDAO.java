package com.usevych.fpdb.dao;

import com.usevych.fpdb.entities.Client;
import org.hibernate.Session;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * DAO to work with Client Entity
 */
public interface ClientDAO {

    /**
     * @return all clients
     */
    public List<Client> getClients();

    /**
     * @param client create new client or update existing
     */
    public void saveClient(Client client);

    /**
     * @param id deleting Client by ID
     */
    public void deleteClient(int id);

    /**
     * @param id
     * @return Client by ID
     */
    public Client getClient(int id);

    /**
     * @param keyword
     * @return List of Clients that contains the keyword in it's vars
     */
    public List<Client> searchClients(String keyword);

    /**
     * @param id increment count of added RiskPersons by Client
     */
    public void incrementClientPeople(int id);

    /**
     * @param id decrement count of added RiskPersons by Client
     */
    public void decrementClientPeople(int id);

    /**
     * @param id
     * @return if client with current id exists
     */
    public boolean clientExists(int id);
}

package com.usevych.fpdb.checker;

import com.usevych.fpdb.entities.RiskPerson;
import com.usevych.fpdb.entities.RiskPersonMatchResult;
import org.apache.commons.math3.util.Precision;
import org.springframework.stereotype.Component;
import org.apache.commons.text.similarity.JaroWinklerDistance;

@Component
public class PersonCheckerImpl implements PersonChecker {

    private JaroWinklerDistance jaroWinklerDistance = new JaroWinklerDistance();

    /**
     * Method comparing both entities and return match result in percentage; It's calculating two coefficients based on object variables fulfilment;
     * First for the final result, and second for separate address checking; In the next step method split checking algorithm into two versions : for US and Others.
     * Its needed because of unique information style for US records and different variables impact;
     * The basics of checking algorithm are Contains or JaroWinklerDistance;
     * Method summarize the information and return it in RiskPersonMatchResult
     *
     * @param inputPerson
     * @param dbPerson
     * @return matchResult
     */

    @Override
    public RiskPersonMatchResult getMatch(RiskPerson inputPerson, RiskPerson dbPerson) {
        double intermResult = 0.0d;
        double mainFinalResult = 0.0d;
        double addressFinalResult = 0.0d;
        double rateCoefficient = 0.0d;
        double addressCoefficient = 0.0d;
        RiskPersonMatchResult matchResult = new RiskPersonMatchResult();
        rateCoefficient = calculateMainCoefficient(inputPerson, dbPerson); //
        addressCoefficient = calculateAddressCoefficient(inputPerson, dbPerson);

        if (isUS(inputPerson)) {
            //FNAME
            intermResult = jaroWinklerDistance.apply(inputPerson.getFirstName().toLowerCase(), dbPerson.getFirstName().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient + 0)); //2
            //LNAME
            intermResult = jaroWinklerDistance.apply(inputPerson.getLastName().toLowerCase(), dbPerson.getLastName().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient + 3)); //3
            // Email
            String inputEmail = clearEmail(inputPerson.getEmail());
            String dbEmail = clearEmail(dbPerson.getEmail());
            intermResult = jaroWinklerDistance.apply(inputEmail, dbEmail);
            mainFinalResult += (intermResult * (rateCoefficient));
            // ADDRESS 1
            String inputAddress = clearAddress(inputPerson.getAddress1());
            String dbAddress = clearAddress(dbPerson.getAddress1());
            intermResult = jaroWinklerDistance.apply(inputAddress, dbAddress);
            mainFinalResult += (intermResult * rateCoefficient);
            addressFinalResult += (intermResult * addressCoefficient);
            // ADDRESS 2
            if (inputPerson.getAddress2() != null && dbPerson.getAddress2() != null) {
                intermResult = jaroWinklerDistance.apply(inputPerson.getAddress2().toLowerCase(), dbPerson.getAddress2().toLowerCase());
                mainFinalResult += (intermResult * (rateCoefficient - 1));
                addressFinalResult += (intermResult * addressCoefficient);
            }
            // CITY
            if (inputPerson.getCity().toLowerCase().equals(dbPerson.getCity().toLowerCase()))
                intermResult = 1.0;
            else
                intermResult = 0.0;
            mainFinalResult += (intermResult * rateCoefficient);
            addressFinalResult += (intermResult * addressCoefficient);
            // STATE
            if (inputPerson.getRegion_state() != null && dbPerson.getRegion_state() != null) {
                if (inputPerson.getRegion_state().toLowerCase().equals(dbPerson.getRegion_state().toLowerCase()))
                    intermResult = 1.0;
                else
                    intermResult = 0.0;
                mainFinalResult += (intermResult * rateCoefficient);
                addressFinalResult += (intermResult * addressCoefficient);
            }
            // ZIP
            intermResult = jaroWinklerDistance.apply(inputPerson.getZip().toLowerCase(), dbPerson.getZip().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient + 1));
            addressFinalResult += (intermResult * addressCoefficient);
        } else {
            //FNAME
            intermResult = jaroWinklerDistance.apply(inputPerson.getFirstName().toLowerCase(), dbPerson.getFirstName().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient + 1));
            //LNAME
            intermResult = jaroWinklerDistance.apply(inputPerson.getLastName().toLowerCase(), dbPerson.getLastName().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient + 1));
            // Email
            intermResult = jaroWinklerDistance.apply(inputPerson.getEmail().toLowerCase(), dbPerson.getEmail().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient - 1));
            // ADDRESS 1
            intermResult = jaroWinklerDistance.apply(inputPerson.getAddress1().toLowerCase(), dbPerson.getAddress1().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient - 1));
            addressFinalResult += (intermResult * addressCoefficient);
            // ADDRESS 2
            if (inputPerson.getAddress2() != null && dbPerson.getAddress2() != null) {
                intermResult = jaroWinklerDistance.apply(inputPerson.getAddress2().toLowerCase(), dbPerson.getAddress2().toLowerCase());
                mainFinalResult += (intermResult * (rateCoefficient - 1));
                addressFinalResult += (intermResult * addressCoefficient);
            }
            // CITY
            intermResult = jaroWinklerDistance.apply(inputPerson.getCity().toLowerCase(), dbPerson.getCity().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient + 1));
            addressFinalResult += (intermResult * addressCoefficient);
            // STATE
            if (inputPerson.getRegion_state() != null && dbPerson.getRegion_state() != null) {
                intermResult = jaroWinklerDistance.apply(inputPerson.getRegion_state().toLowerCase(), dbPerson.getRegion_state().toLowerCase());
                mainFinalResult += (intermResult * rateCoefficient);
                addressFinalResult += (intermResult * addressCoefficient);
            }
            // ZIP
            intermResult = jaroWinklerDistance.apply(inputPerson.getZip().toLowerCase(), dbPerson.getZip().toLowerCase());
            mainFinalResult += (intermResult * (rateCoefficient - 1));
            addressFinalResult += (intermResult * addressCoefficient);
            // COUNTRY
            if (inputPerson.getCity().toLowerCase().equals(dbPerson.getCity().toLowerCase()))
                intermResult = 1.0;
            else
                intermResult = 0.0;
            mainFinalResult += (intermResult * (rateCoefficient + 1));
            addressFinalResult += (intermResult * addressCoefficient);
        }
        mainFinalResult = Precision.round(mainFinalResult, 2);
        addressFinalResult = Precision.round(addressFinalResult, 2);
        matchResult.setMainMatchResult(mainFinalResult);
        matchResult.setAddressMatchResult(addressFinalResult);
        return matchResult;
    }



    private boolean isUS(RiskPerson person) {
        if (person.getCountry().toLowerCase().equals("us") || person.getCountry().toLowerCase().equals("united states")) {
            return true;
        } else
            return false;
    }


    /**
     * Method calculates max final coefficient based on field filled
     * @param inputPerson
     * @param dbPerson
     * @return
     */
    private double calculateMainCoefficient(RiskPerson inputPerson, RiskPerson dbPerson) {
        int count = 0;
        double coefficient;

        if ((inputPerson.getFirstName() != null) && (dbPerson.getFirstName() != null))
            count++;
        if ((inputPerson.getLastName() != null) && (dbPerson.getLastName() != null))
            count++;
        if ((inputPerson.getEmail() != null) && (dbPerson.getEmail() != null))
            count++;
        if ((inputPerson.getAddress1() != null) && (dbPerson.getAddress1() != null))
            count++;
        if ((inputPerson.getAddress2() != null) && (dbPerson.getAddress2() != null))
            count++;
        if ((inputPerson.getCity() != null) && (dbPerson.getCity() != null))
            count++;
        if ((inputPerson.getRegion_state() != null) && (dbPerson.getRegion_state() != null))
            count++;
        if ((inputPerson.getZip() != null) && (dbPerson.getZip() != null))
            count++;
        if (!isUS(inputPerson) && !isUS(dbPerson)) {
            if ((inputPerson.getCountry() != null) && (dbPerson.getCountry() != null))
                count++;
        }

        coefficient = (100.0 / count);
        return coefficient;
    }

    /**
     * Method calculates max address coefficient based on field filled
     * @param inputPerson
     * @param dbPerson
     * @return
     */
    private double calculateAddressCoefficient(RiskPerson inputPerson, RiskPerson dbPerson) {
        int count = 0;
        double coefficient;
        if ((inputPerson.getAddress1() != null) && (dbPerson.getAddress1() != null))
            count++;
        if ((inputPerson.getAddress2() != null) && (dbPerson.getAddress2() != null))
            count++;
        if ((inputPerson.getCity() != null) && (dbPerson.getCity() != null))
            count++;
        if ((inputPerson.getRegion_state() != null) && (dbPerson.getRegion_state() != null))
            count++;
        if ((inputPerson.getZip() != null) && (dbPerson.getZip() != null))
            count++;
        if (!isUS(inputPerson) && !isUS(dbPerson)) {
            if ((inputPerson.getCountry() != null) && (dbPerson.getCountry() != null))
                count++;
        }
        coefficient = (100.0 / count);
        return coefficient;
    }


    /**
     * Method clear the addresses fields from garbage or repeatable information for JaroWinklerDistance to have the more accurate result
     * @param address
     * @return
     */
    private String clearAddress(String address) {
        String[] insideKeyWords = {" str\\.", " ave ", " ave\\.", " suite "};
        String[] endsKeyWords = {" st", " st.", " street.", " street", " str", " road"};
        String result = address.toLowerCase();
        for (String replacement : insideKeyWords) {
            result = result.replaceAll(replacement, "");
        }
        for (String replacement : endsKeyWords) {
            if (result.endsWith(replacement)) {
                result = result.substring(0, result.lastIndexOf(replacement));
            }
        }
        return result;
    }

    private String clearEmail(String email) {
        String result;
        result = email.toLowerCase();
        if (result.contains("@")) {
            result = result.substring(0, result.indexOf("@"));
        }
        return result;
    }

}

package com.usevych.fpdb.checker;

import com.usevych.fpdb.entities.RiskPerson;
import com.usevych.fpdb.entities.RiskPersonMatchResult;

/**
 *  Interface for comparing both entities and returning result
 */
public interface PersonChecker {

    public RiskPersonMatchResult getMatch(RiskPerson inputPerson, RiskPerson dbPerson);
}

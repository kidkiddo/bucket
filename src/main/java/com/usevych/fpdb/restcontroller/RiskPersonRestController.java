package com.usevych.fpdb.restcontroller;


import com.usevych.fpdb.checker.PersonChecker;
import com.usevych.fpdb.dao.RiskPersonDAO;
import com.usevych.fpdb.entities.Client;
import com.usevych.fpdb.entities.RiskPerson;
import com.usevych.fpdb.entities.RiskPersonMatchResult;
import com.usevych.fpdb.service.ClientService;
import com.usevych.fpdb.service.RiskPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class RiskPersonRestController {

    @Autowired
    private RiskPersonService riskPersonService;

    @Autowired
    private PersonChecker personChecker;

    //http://localhost:8080/api/check?fnm=Rosana&mnm=Upton&lnm=Paulich&eml=upaulich2n@utexas.edu&addr1=396%20Towne%20Alley&addr2=4%20Oakridge%20Terrace&rgst=Ohio&cty=Dayton&zip=33333&cntry=US


    @GetMapping(value = "/check", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity matchClient(@RequestParam("fnm") String firstName, @RequestParam("mnm") String middleName,
                                      @RequestParam("lnm") String lastName, @RequestParam("eml") String email,
                                      @RequestParam("addr1") String address1, @RequestParam("addr2") String address2,
                                      @RequestParam("rgst") String region_state, @RequestParam("cty") String city,
                                      @RequestParam("zip") String zip, @RequestParam("cntry") String country) {
        RiskPersonMatchResult finalResultObj = new RiskPersonMatchResult();
        RiskPerson riskPerson = new RiskPerson();

        riskPerson.setEmail(email);
        riskPerson.setFirstName(firstName);
        riskPerson.setMiddleName(middleName);
        riskPerson.setLastName(lastName);
        riskPerson.setAddress1(address1);
        riskPerson.setAddress2(address2);
        riskPerson.setRegion_state(region_state);
        riskPerson.setCity(city);
        riskPerson.setZip(zip);
        riskPerson.setReason("d");
        riskPerson.setCountry(country);
        finalResultObj = riskPersonService.searchSimilarPeople(riskPerson);
        return new ResponseEntity(finalResultObj, HttpStatus.OK);
    }


}

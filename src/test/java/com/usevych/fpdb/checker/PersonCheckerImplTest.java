package com.usevych.fpdb.checker;

import com.usevych.fpdb.entities.RiskPerson;
import com.usevych.fpdb.entities.RiskPersonMatchResult;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.*;

public class PersonCheckerImplTest {

    private static PersonCheckerImpl personChecker = new PersonCheckerImpl();
    private static ArrayList<RiskPerson> inputList = new ArrayList<>();
    private static ArrayList<RiskPerson> dbList = new ArrayList<>();

    public static String generateRandomChars(String candidateChars, int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars
                    .length())));
        }

        return sb.toString();
    }


    /**
     * Simulation of changing the data to conceal record existing in DB
     * @param field
     * @param countOfSymbols
     * @param method
     * @return distorted String
     */
    public String distortField(String field, int countOfSymbols, int method) { // 0 Remove, 1 Add, 2 Random
        String result = field;
        Random random = new Random();
        int number = method;
        if (method == 2) {
            number = random.nextInt(2);
        }
            switch (number) {
                case 0:
                    number = 0; //Remove random symbols
                    while (field.length() < countOfSymbols) {
                        countOfSymbols--;
                    }
                    result = field.substring(0, field.length() - countOfSymbols);
                    break;

                case 1:
                    number = 1; // Add random symbols
                    String generated = generateRandomChars("abcdefghijklmnopqrstuvwxyz", countOfSymbols);
                    result = field + generated;
                    break;
            }
        return result;
    }

    @Before
    public void initTest() throws IOException {
        PersonChecker personChecker = new PersonCheckerImpl();
        String line;
        BufferedReader bf = null;
        try {
            bf = new BufferedReader(new FileReader("D:\\Java/Learning/FPDB-0.1/src/test/java/com/usevych/fpdb/checker/MainTestData_ru.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while ((line = bf.readLine()) != null) {
            RiskPerson riskPerson = new RiskPerson();
            String[] array = line.split(",");
            riskPerson.setEmail(array[0]);
            riskPerson.setFirstName(array[1]);
            if (!array[2].isEmpty()) {
                riskPerson.setMiddleName(array[2]);
            }
            riskPerson.setLastName(array[3]);
            riskPerson.setAddress1(array[4]);
            if (!array[5].isEmpty()) {
                riskPerson.setAddress2(array[5]);
            }
            riskPerson.setRegion_state(array[6]);
            riskPerson.setCity(array[7]);
            riskPerson.setZip(array[8]);
            riskPerson.setCountry(array[9]);
            inputList.add(riskPerson);

            RiskPerson riskPersonCopy = null;
            try {
                riskPersonCopy = (RiskPerson) riskPerson.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            riskPersonCopy.setFirstName(distortField(riskPerson.getFirstName(), 2,0));
            riskPersonCopy.setLastName(distortField(riskPerson.getLastName(), 2,0));
            riskPersonCopy.setAddress1(distortField(riskPersonCopy.getAddress1(), 3,0));
            riskPersonCopy.setZip(distortField(riskPersonCopy.getZip(),2,0));
            riskPersonCopy.setEmail(distortField(riskPerson.getEmail(), 5,0));
            dbList.add(riskPersonCopy);
        }
        bf.close();
    }

    @Test
    public void getMatch() {
        double[] matchResults = new double[inputList.size()];
        double avarage = 0.0d;
        for (int i = 0; i < inputList.size(); i++) {
            RiskPersonMatchResult result = personChecker.getMatch(inputList.get(i), dbList.get(i));
            double current = result.getMainMatchResult();
            matchResults[i] = current;
        }
        for (int i = 0; i < matchResults.length; i++) {
            avarage = avarage + matchResults[i];
        }
        avarage = avarage / matchResults.length;
        double expected = 85;
        System.out.println(avarage);
        assertTrue(avarage >= expected);
    }
}